package routes

import (
	"song-library/controllers"

	"github.com/gofiber/fiber/v2"
)

func UserRoutes(app *fiber.App) {

	// Routes of an user
	userApi := app.Group("/users")

	userApi.Get("/", controllers.GetUsers)
	userApi.Post("/", controllers.AddUser)
	userApi.Get("/:id<int>", controllers.GetUser)

}
