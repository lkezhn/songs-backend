package models

import "gorm.io/gorm"

type User struct {
	gorm.Model

	FirstName string `json:"firstname" gorm:"type:varchar(100);not null"`
	LastName  string `json:"lastname" gorm:"type:varchar(100);not null"`
	Email     string `json:"email" gorm:"type:varchar(100);unique_index;not null"`
	Username  string `json:"username" gorm:"type:varchar(25);unique_index;not null"`
	Password  string `json:"password" gorm:"type:varchar(100);not null"`
}
