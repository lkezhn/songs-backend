package services

import (
	"encoding/json"
	"log"

	"song-library/configs"
	"song-library/models"
	"song-library/utils"
)

func GetUsers() []models.User {

	var users []models.User
	configs.DB.Find(&users)

	return users
}

func GetUser(id string) models.User {

	var user models.User
	configs.DB.First(&user, "id = ?", id)

	return user
}

func AddUser(userInfo []byte) {

	var newUser models.User

	err := json.Unmarshal(userInfo, &newUser)
	if err != nil {
		log.Fatal(err)
	}

	newUser.Password, err = utils.HashPassword(newUser.Password)
	if err != nil {
		log.Fatal(err)
	}

	res := configs.DB.Create(&newUser)
	if res.Error != nil {
		log.Fatal(res.Error)
	}

}
