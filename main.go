package main

import (
	"song-library/configs"
	"song-library/models"
	"song-library/routes"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
)

func main() {

	configs.GetConn()

	configs.DB.AutoMigrate(&models.User{})

	app := fiber.New()

	app.Use(cors.New())
	app.Use(logger.New())

	routes.UserRoutes(app)

	app.Get("/", func(c *fiber.Ctx) error {
		return c.Status(fiber.StatusOK).JSON(&fiber.Map{"data": "Fiber API"})
	})

	app.Listen(":3000")
}
