package controllers

import (
	"song-library/services"

	"github.com/gofiber/fiber/v2"
)

func GetUsers(c *fiber.Ctx) error {

	userList := services.GetUsers()

	return c.Status(fiber.StatusOK).JSON(userList)
}

func GetUser(c *fiber.Ctx) error {

	user := services.GetUser(c.Params("id"))

	if user.ID == 0 {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"message": "User not found."})
	}

	return c.Status(fiber.StatusOK).JSON(user)
}

func AddUser(c *fiber.Ctx) error {

	services.AddUser(c.Body())

	return c.Status(fiber.StatusCreated).JSON(fiber.Map{"message": "User created"})
}
